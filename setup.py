import os
import sys

from setuptools import find_packages, setup

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

python_version = sys.version_info
python_version = '{}.{}'.format(python_version.major, python_version.minor)
install_requires = {
    '3.7': [
        'Django==1.8.19',
    ],
}

setup(
    name='dynamodb_mapper',
    version='1.8.2',
    python_requires='!=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, !=3.5.*, !=3.6.*, <4',  # noqa
    packages=find_packages(exclude=["*.swp"]),
    install_requires=install_requires[python_version],
    include_package_data=True,
    license='Not open source',
    description='dynamodb mapper',
    url='https://pypi.org/project/dynamodb-mapper/',
    author='BeeFee',
    author_email='programmers@beefee.co.uk',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.7',
    ],
)
